# Factorio Robot City Scenario

A Factorio freeplay scenario to build a city of robots.


## Objective

Build a Robot City with a population of over 500,000 robots.


## Notes

1. Resources should **NEVER** be an issue.  Each plot of resources is more than ten times what you'll need to achieve the objective.
2. A Spidertron is provided at the beginning of the scenario because you'll probably be moving around a lot.  Also, you'll probably build a city where foot traffic is impossible (blocked by solar panels as well as other non-passable items).  Since this is a "robot only" city I viewed my character as the Spidertron itself instead of it just being a vehicle.
3. I strongly recommend some sort of **long reach** mod.  In addition, it is my preference to also use some sort of **afraid of the dark** mod.
4. You don't **HAVE** to use the shadowed power poles and Roboports.  They just provide a suggested design for the city.  I figure if robots were to design a city it would be *perfectly* square and leave no space unused.  That said, there are more than enough Roboports to house 500,000 robots in this design.


## Extra Challenges

1. This is a city of flying robots so try to never use any belts.
2. Minimize your pipe usage to the bare minimum.  The main limitation, for me, was providing water to the Nuclear Power Plants.
3. Build a *City Block* worth of every possible item.  The [RobotCityPlanner.txt](./RobotCityPlanner.txt) should have a city block of every item.


## How to use

1. Clone the repo: `git clone git@gitlab.com:daxm/factorio-robot-city.git`
2. Copy and paste the **Robot City** folder into your Factorio's scenario folder and then load that scenario when starting a new game.


## TODOs

* I don't know how to program in Lua, yet.  I'd like to modify the winning endpoint to the 500,000 robot count instead of putting a satellite into space.
* Modify the description and intro instructions to explain the objective.
* Actually deny the ability to place belts.
* Instead of surrounding the base with dense trees maybe redo the environment to just have a water moat so that enemies can't cross.  This would allow the enemies to build closer to the city and be more fun in the hunting.  As it is now I have to go real far to find bases.
* Re-arrange the resource blocks.  Though it is convenient in the beginning to have the Iron and Copper resources close together they become a major bottleneck in the later stages of the game as bots need to recharge and the Roboports in that area are always full.
  
Pull requests are welcomed!  :-)
